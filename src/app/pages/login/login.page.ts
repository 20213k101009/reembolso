import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  constructor(private router: Router) {}

  login() {
    console.log('Inicio de sesión...');

    this.router.navigate(['code']);

  }

  signup() {
    console.log('Inicio de sesión...');

    this.router.navigate(['signup']);

  }

}
