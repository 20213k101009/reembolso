import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pass',
  templateUrl: './pass.page.html',
  styleUrls: ['./pass.page.scss'],
})
export class PassPage  {


  constructor(private router: Router) {}

  send() {
    console.log('Inicio de sesión...');

    this.router.navigate(['home']);

  }

}
