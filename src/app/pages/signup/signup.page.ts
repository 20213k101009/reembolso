import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage  {

  constructor(private router: Router) {}

  login() {
    console.log('Inicio de sesión...');

    this.router.navigate(['code']);

  }

  signup() {
    console.log('Inicio de sesión...');

    this.router.navigate(['login']);

  }

}
