import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  phoneForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.phoneForm = this.formBuilder.group({
      phoneNumber: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]]
    });
  }

  onSubmit() {
    if (this.phoneForm.valid) {
      console.log('Número de teléfono ingresado:', this.phoneForm.value.phoneNumber);
    }
  }

  

  ngOnInit() {
  }

}
